<?php

namespace Drupal\add_content_modal\Theme;

use Drupal\add_content_modal\Form\SettingsForm;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Sets the admin theme on modals if wanted.
 */
class ThemeNegotiator implements ThemeNegotiatorInterface {

  use AjaxHelperTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new ThemeNegotiator instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $this->getActiveTheme($route_match) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->getActiveTheme($route_match);
  }

  /**
   * Determine the active theme for the current route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return string
   *   The active theme or an empty string.
   */
  protected function getActiveTheme(RouteMatchInterface $route_match) {
    $config = $this->configFactory->get(SettingsForm::SETTINGSNAME);
    $dialogAdminTheme = $config->get('dialog_admin_theme', FALSE);

    if (!$dialogAdminTheme) {
      return '';
    }

    $routeObject = $route_match->getRouteObject();
    if (empty($routeObject)) {
      return '';
    }

    if ($this->isAjax()) {
      return $this->configFactory->get('system.theme')->get('admin');
    }

    return '';
  }

}
